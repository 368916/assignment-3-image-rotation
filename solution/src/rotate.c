#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "transform.h"
#include "image.h"

static uint64_t get_index(uint64_t x, uint64_t y,
		uint64_t width, uint64_t height, bool is_rotated)
{
	return !is_rotated ? y * width + x : y * height + x;
}

static void rotate_coords(uint64_t *x, uint64_t *y,
		uint64_t width, uint64_t height, uint64_t rotations)
{
	uint64_t nx = *x, ny = *y;

	switch (rotations) {
		case 1:
			nx = *y;
			ny = (width - 1) - *x;
			break;
		case 2:
			nx = (width - 1) - *x;
			ny = (height - 1) - *y;
			break;
		case 3:
			nx = (height - 1) - *y;
			ny = *x;
			break;
		default:
			break;
	}

	*x = nx;
	*y = ny;
}

static void rotate_pixel(struct pixel new[], struct pixel old[],
		uint64_t width, uint64_t height, uint64_t rotations,
		uint64_t x, uint64_t y)
{
	const bool is_rotated = rotations % 2 != 0;

	uint64_t a = x, b = y;

	uint64_t index = get_index(a, b, width, height, false);
	struct pixel pixel = old[index];

	rotate_coords(&a, &b, width, height, rotations);

	index = get_index(a, b, width, height, is_rotated);

	new[index] = pixel;
}

static enum transform_status rotate(struct image *const image, const void *argv)
{
	const int64_t *angle = argv;
	const uint64_t rotations = (*angle >= 0 ? *angle : 360 + *angle) / 90;

	const bool is_rotated = rotations % 2 != 0;

	void *buffer = image_duplicate_buffer(image);
	if (buffer == NULL)
		return TRANSFORM_ERR;

	const uint64_t width = image->width;
	const uint64_t height = image->height;
	for (uint64_t y = 0; y < height; y++) {
		for (uint64_t x = 0; x < width; x++) {
			rotate_pixel(buffer, image->data,
					width, height, rotations, x, y);
		}
	}

	free(image->data);
	image->data = buffer;

	if (is_rotated) {
		const uint64_t tmp = image->width;
		image->width = image->height;
		image->height = tmp;
	}

	return TRANSFORM_OK;
}

struct transform rotate_transform = {
	.f = rotate
};

