#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "file.h"

#include "format.h"
#include "transform.h"

extern struct format bmp_format;
extern struct transform rotate_transform;

int main(int argc, char *argv[])
{
	if (argc < 4) {
		printf("Usage: ./image-transformer <source-image> <transformed-image> <angle>\n");
		return -1;
	}

	const char *source_image = argv[1];
	const char *transformed_image = argv[2];
	const int64_t angle = atoi(argv[3]);
	if (angle % 90 != 0) {
		perror("Unsupported angle. Should be divisible by 90.\n");
		return -1;
	}

	FILE *file = fopen(source_image, "rb");
	uint8_t *content = NULL;
	if (load_file_content(file, &content)) {
		perror("Failed to read image content\n.");
		return -1;
	}

	struct image image;
	enum decode_status dec_status = bmp_format.decode(content, &image);
	free(content);
	if (dec_status != DECODE_OK) {
		perror("Failed to decode image.\n");
		return -1;
	}

	rotate_transform.f(&image, (void *) &angle);

	uint8_t *buffer = NULL;
	uint64_t size = 0;
	enum encode_status enc_status = bmp_format.encode(&image, &buffer, &size);
	free(image.data);
	if (enc_status != ENCODE_OK) {
		perror("Failed to encode image.\n");
		return -1;
	}

	FILE *new_file = fopen(transformed_image, "wb");
	if (fwrite(buffer, size, 1, new_file) < 1) {
		perror("Failed to write image.\n");
		return -1;
	}
	free(buffer);

	fclose(new_file);
}

