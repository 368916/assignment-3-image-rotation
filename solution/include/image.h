#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) pixel {
	uint8_t r, g, b;
};

struct image {
	uint64_t width, height;
	struct pixel *data;
};

uint64_t image_get_size(const struct image *image);
void* image_duplicate_buffer(const struct image *image);

#endif
