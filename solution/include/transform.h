#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

enum transform_status {
	TRANSFORM_OK = 0,
	TRANSFORM_ERR
};

struct transform {
	enum transform_status (*f)(struct image *const image, const void *argv);
};

#endif

