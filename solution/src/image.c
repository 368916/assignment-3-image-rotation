#include "image.h"

#include <stdlib.h>

uint64_t image_get_size(const struct image *image)
{
	if (!image)
		return 0;

	const uint64_t width = image->width;
	const uint64_t height = image->height;

	const uint64_t pixel_cnt = width * height;

	return pixel_cnt * sizeof(struct pixel);
}

void* image_duplicate_buffer(const struct image *image)
{
	if (!image)
		return NULL;

	uint64_t size = image_get_size(image);
	if (size == 0)
		return NULL;

	return malloc(size);
}

