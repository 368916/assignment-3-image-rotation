#ifndef FILE_H
#define FILE_H

#include <stdint.h>
#include <stdio.h>

int64_t get_file_size(FILE *file);
int64_t load_file_content(FILE *file, uint8_t **buffer);

#endif
