#ifndef FORMAT_H
#define FORMAT_H

#include <stdint.h>

#include "image.h"

enum decode_status {
	DECODE_OK = 0,
	DECODE_ERR
};

enum encode_status {
	ENCODE_OK = 0,
	ENCODE_ERR
};

struct format {
	enum decode_status (*decode)(const void *buffer, struct image *image);
	enum encode_status (*encode)(const struct image *image, uint8_t **buffer, uint64_t *size);
};

#endif
