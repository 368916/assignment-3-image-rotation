#include "file.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int64_t get_file_size(FILE *file)
{
	fseek(file, 0, SEEK_END);
	const int64_t size = ftell(file);
	fseek(file, 0, SEEK_SET);

	return size;
}

int64_t load_file_content(FILE *file, uint8_t **buffer)
{
	const int64_t file_size = get_file_size(file);

	*buffer = malloc(file_size * sizeof(uint8_t));
	if (fread(*buffer, file_size, 1, file) < 1)
		return -1;

	return 0;
}
