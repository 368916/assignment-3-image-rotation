#include <stdint.h>
#include <stdlib.h>

#include "format.h"
#include "image.h"

#define BMP_FILE_TYPE_LE 0x4d42
#define BMP_HEADER_SIZE 40

#define ROW_ALIGNMENT 4

struct __attribute__((packed)) bmp_header {
	struct __attribute__((packed)) {
		uint16_t type;
		uint32_t size;
		uint32_t reserved;
		uint32_t offset;
	} bf;

	struct __attribute__((packed)) {
		uint32_t size;
		uint32_t width;
		uint32_t height;
		uint16_t planes;
		uint16_t bit_count;
		uint32_t compression;
		uint32_t size_image;
		uint32_t x_pels_per_metre;
		uint32_t y_pels_per_metre;
		uint32_t clr_used;
		uint32_t clr_important;
	} bi;
};

static uint64_t get_padding(uint64_t width)
{
	uint64_t width_bytes = width * sizeof(struct pixel);

	if (width_bytes % ROW_ALIGNMENT != 0)
		return (width_bytes / ROW_ALIGNMENT + 1) * ROW_ALIGNMENT - width_bytes;

	return 0;		
}

static uint64_t get_row_length_bytes(uint64_t width)
{
	return width * sizeof(struct pixel) + get_padding(width);
}

static struct pixel* bmp_decode_get_pixel(const struct bmp_header *header,
						uint64_t x, uint64_t y)
{
	// Pixel data lies at 'header start' + 'bf.offset'
	const uint8_t *pixels_data = (uint8_t *) header + header->bf.offset;
	const uint64_t pixel_row_bytes = get_row_length_bytes(header->bi.width);

	const uint64_t shift_x = x * sizeof(struct pixel);
	const uint64_t shift_y = y * pixel_row_bytes;

	struct pixel* pixel = (struct pixel *) (pixels_data + shift_x + shift_y);

	// b, g, r -> r, g, b
	uint8_t tmp = pixel->r;
	pixel->r = pixel->b;
	pixel->b = tmp;

	return pixel;
}

static void bmp_decode_fill_dims(const struct bmp_header *header,
		struct image *const image)
{
	image->width = header->bi.width;
	image->height = header->bi.height;
}

static void bmp_decode_fill_pixels(const struct bmp_header *header,
		struct pixel *pixels, struct image *const image)
{
	const uint64_t width = image->width;
	const uint64_t height = image->height;

	image->data = pixels;

	for (uint64_t y = 0; y < height; y++) {
		for (uint64_t x = 0; x < width; x++) {
			const uint64_t index = y * width + x;
			image->data[index] = *bmp_decode_get_pixel(header, x, y);
		}
	}
}

static enum decode_status bmp_decode_validate(const struct bmp_header *header)
{
	if (header->bf.type != BMP_FILE_TYPE_LE)
		return DECODE_ERR;

	if (header->bi.size != BMP_HEADER_SIZE)
		return DECODE_ERR;

	return DECODE_OK;
}

static enum decode_status bmp_decode(const void *buffer, struct image *const image)
{
	enum decode_status status;

	if (!buffer || !image)
		return DECODE_ERR;

	// Header lies at the beggining of buffer, so just casting pointer
	const struct bmp_header *header = buffer;

	status = bmp_decode_validate(header);
	if (status != DECODE_OK)
		return status;

	bmp_decode_fill_dims(header, image);

	struct pixel *pixels = image_duplicate_buffer(image);
	if (pixels == NULL) {
		status = DECODE_ERR;
		return status;
	}

	bmp_decode_fill_pixels(header, pixels, image);

	return DECODE_OK;
}

static void bmp_encode_set_pixel(const struct image *image,
		uint8_t *buffer, uint64_t x, uint64_t y, struct pixel pixel)
{
	const uint64_t pixel_row_bytes = get_row_length_bytes(image->width);

	const uint64_t shift_x = x * sizeof(struct pixel);
	const uint64_t shift_y = y * pixel_row_bytes;

	uint8_t *position = buffer + shift_x + shift_y;
	*(position + 0) = pixel.b;
	*(position + 1) = pixel.g;
	*(position + 2) = pixel.r;
}

static void* bmp_encode_allocate_image_data(const struct image *image,
		uint64_t *size)
{
	const uint64_t width = image->width;
	const uint64_t height = image->height;

	const uint64_t pixel_row_bytes = get_row_length_bytes(width);

	const uint64_t header_size = sizeof(struct bmp_header);
	const uint64_t pixel_data_size = pixel_row_bytes * height;

	const uint64_t required_size = header_size + pixel_data_size;
	*size = required_size;

	return malloc(required_size * sizeof(uint8_t));
}

static uint64_t bmp_encode_fill_header(const struct image *image,
		uint8_t *buffer, uint64_t size)
{
	static const uint64_t BMP_HEADER_PLANES = 1;
	static const uint64_t BMP_HEADER_BIT_COUNT = 24;

	struct bmp_header *header = (void *) buffer;
	header->bf.type = BMP_FILE_TYPE_LE;
	header->bf.size = size;
	header->bf.reserved = 0;
	header->bf.offset = sizeof(struct bmp_header);

	header->bi.size = BMP_HEADER_SIZE;
	header->bi.width = image->width;
	header->bi.height = image->height;
	header->bi.planes = BMP_HEADER_PLANES;
	header->bi.bit_count = BMP_HEADER_BIT_COUNT;
	header->bi.compression = 0;
	header->bi.size_image = 0;
	header->bi.x_pels_per_metre = 0;
	header->bi.y_pels_per_metre = 0;
	header->bi.clr_used = 0;
	header->bi.clr_important = 0;

	return header->bf.offset;
}

static void bmp_encode_fill_pixels(const struct image *image, uint8_t *buffer)
{
	const uint64_t width = image->width;
	const uint64_t height = image->height;

	const uint64_t padding = get_padding(width);

	for (uint64_t y = 0; y < height; y++) {
		for (uint64_t x = 0; x < width; x++) {
			const uint64_t index = y * width + x;
			const struct pixel pixel = image->data[index];

			bmp_encode_set_pixel(image, buffer, x, y, pixel);
		}

		const uint64_t offset = (y + 1) * get_row_length_bytes(width) - padding;
		uint8_t *row_end = buffer + offset;
		for (uint64_t x = 0; x < padding; x++) {
			*(row_end + x) = 0;
		}
	}
}

static enum encode_status bmp_encode(const struct image *image,
		uint8_t **buffer, uint64_t *size)
{
	if (!image)
		return ENCODE_ERR;

	*buffer = bmp_encode_allocate_image_data(image, size);
	if (!buffer)
		return ENCODE_ERR;

	const uint64_t offset = bmp_encode_fill_header(image, *buffer, *size);

	uint8_t *buffer_with_offset = *buffer + offset;
	bmp_encode_fill_pixels(image, buffer_with_offset);

	return ENCODE_OK;
}

struct format bmp_format = {
	.decode = bmp_decode,
	.encode = bmp_encode,
};

